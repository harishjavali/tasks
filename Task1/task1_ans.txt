﻿                             TASK-1
    1. Disk usage statistics for root folder (available diskspace, used and free in GBs) (hint: df)
->df -h
[disk filesystem h-human readable]
    2. Memory (RAM) usage statistics (total memory available, used, and free in MBs) (hint: free)
->free -h
[free-to display used and used memory, h-human readble]
    3. What process is using the max memory? (hint: top)
->top then enter M.
[top-to show real running processes status and M- sort according Memory]
    4. Find the chrome processes currently running (hint: ps, grep)
->ps -ef | grep chrome
[ps-actice process sate -e-all process states f-full info| grep chrome-text search(to extarct chrome processes)
    5. Which folder or file in /usr/bin is taking most amount of diskspace (hint: du, sort)
->du -ah | sort -h -r | head -1
[du-disk usage a-usage count of all files h-human readable | sort -h- sort according human readable size | head -1- prints which has most amount of disk usage]
    6. Find the total number of chrome process running (hint: wc)
->ps -e | grep chrome | wc -l
[ps-actice process sate -e-all process states | grep chrome-text search(to extarct chrome processes)| wc -l -counts active chrome processes]
    7. Find the RSS (Resident non-swapped memory usage) for each process (hint: ps options)
->ps -aux –-sort -rss
[ps -aux-all process actice state(we can also use ps -ef) | sort -rss- for sorting]
    8.  Get the pids of all chrome process currently running (hint: ps, grep, cut)
->ps -e |grep chrome | cut -f 3-4 -d ‘ ’
[ps-actice process sate -e-all process states | grep chrome-text search(to extarct chrome processes)| cut -f -cut fields, d – delimiter – to get only pid numbers]
    9. Write a script that will log the memory utilization to a file along with current data and time. Every time script is run, the information should be added to the same file.  Figure out how this script can be scheduled so that it will automatically run every 10 min and we can have memory util info for every 10 min (hint: date, crontab)
       #!/bin/bash
	echo "date and time:"$(date | cut -f 2-7 -d " ")>>h1.txt
	echo "-----------------------------------------------------">>h1.txt
	echo "memory utilization info:->">>h1.txt
	echo "-----------------------------------------------------">>h1.txt
	free -h >>h1.txt
	echo " ">>h1.txt
	echo"------------------------------------------------------------">>h1.txt

	crontab -e
      */10 * * * *  ~/log.ch
